using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using SeleniumTest.PageObjects;
using Xunit;


namespace SeleniumTest
{
    public class Registration
    {
        public IWebDriver chrome;

        [Fact]
        public void LaunchBrowser()
        {
            chrome = new ChromeDriver();
            chrome.Navigate().GoToUrl("https://newbookmodels.com/");
            //chrome.Manage().Window.Maximize();
            chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }

        [Theory]
        [InlineData("Jack", "Daniels", "debocit979@bio123.net", "JDoo12345*", "JDoo12345*", "18327350678")]
        public void RegistrationPage1ValidData(string name, string lastname, string email, string password, string confirmpassword, string mobile)
        {
            RegistrationPage1 registrationPage = new RegistrationPage1();
            registrationPage.btnSignUp.Click();
            registrationPage.firstName.SendKeys(name);
            registrationPage.lastName.SendKeys(lastname);
            registrationPage.email.SendKeys(email);
            registrationPage.password.SendKeys(password);
            registrationPage.passwordConfirm.SendKeys(confirmpassword);
            registrationPage.phoneNumber.SendKeys(mobile);
            registrationPage.btnSubmit.Click();
        }

        [Theory]
        [InlineData("DevQA", "https://google.com", "Ukraine Way, Boston, MA, USA")]
        public void RegistrationPage2ValidData(string companyname, string compwebsite, string adress)
        {
            RegistrationPage2 registrationPage2 = new RegistrationPage2();
            registrationPage2.companyName.SendKeys(companyname);
            registrationPage2.companySite.SendKeys(compwebsite);
            registrationPage2.location.SendKeys(adress);
            registrationPage2.industry.Click();
            registrationPage2.selectIndustry.Click();
            registrationPage2.btnFinish.Click();
        }

        [Fact]
        public void ClosedBrowser()
        {
            chrome.Quit();
        }
    }

}
