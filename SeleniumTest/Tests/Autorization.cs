using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using SeleniumTest.PageObjects;
using Xunit;

namespace SeleniumTest
{
    public class Autorization
    {
        public IWebDriver chrome;

        [Fact]
        public void LaunchBrowser()
        {
            chrome = new ChromeDriver();
            chrome.Navigate().GoToUrl("https://newbookmodels.com/");
            //chrome.Manage().Window.Maximize();
            chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
        }

        [Theory]
        [InlineData ("", "")]
        [InlineData("", "1234Abcd*")]
        [InlineData("TestAdmin", "")]
        [InlineData("TestAdmin", "1234Abcd*")]
        public void AutorizationNoValidData(string email, string password)
        {
            AutorizationPage autorizationPage = new AutorizationPage();
            autorizationPage.btnLogIn.Click();
            autorizationPage.inputEmail.SendKeys(email);
            autorizationPage.inputPassword.SendKeys(password);
            autorizationPage.pressBtnLogIn.Click();
        }

        [Theory]
        [InlineData ("jkhkjh @gmail.com", "JDoo12345*")]
        public void AutorizationValidData(string email, string password)
        {
            AutorizationPage autorizationPage = new AutorizationPage();
            autorizationPage.btnLogIn.Click();
            autorizationPage.inputEmail.SendKeys(email);
            autorizationPage.inputPassword.SendKeys(password);
            autorizationPage.pressBtnLogIn.Click();
        }

        [Fact]
        public void CloseBrowser()
        {
            chrome.Quit();
        }
    }
}
