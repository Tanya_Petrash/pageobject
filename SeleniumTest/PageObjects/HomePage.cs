﻿using OpenQA.Selenium;

namespace SeleniumTest.PageObjects
{
    class HomePage
    {
        IWebDriver chrome;
        private readonly By btnSignUp = By.ClassName("Navbar__signUp--12ZDV");

        public HomePage(IWebDriver chrome)
        {
            this.chrome = chrome;
        }

        public void SignUp()
        {
            chrome.FindElement(btnSignUp).Click();
        }
    }
}
