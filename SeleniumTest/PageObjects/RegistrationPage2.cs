﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTest.PageObjects
{
    class RegistrationPage2
    {
        IWebDriver chrome;

        public RegistrationPage2(IWebDriver chrome)
        {
            this.chrome = chrome;
        }

        [FindsBy(How = How.Name, Using = "company_name")]
        public IWebElement companyName { get; set; }

        [FindsBy(How = How.Name, Using = "company_website")]
        public IWebElement companySite { get; set; }

        [FindsBy(How = How.Name, Using = "location")]
        public IWebElement location { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='Select__valueBox--2VlHF']")]
        public IWebElement industry { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[4]/span")]
        public IWebElement selectIndustry { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public IWebElement btnFinish { get; set; }
    }
}
