﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTest.PageObjects
{
    public class AutorizationPage
    {
        IWebDriver chrome;

        public AutorizationPage(IWebDriver chrome)
        {
            this.chrome = chrome;
        }

        [FindsBy(How = How.XPath, Using = "/html/body/nb-app/nb-home-page/common-react-bridge/div/header/span[1]/a[2]")]
        public IWebElement btnLogIn { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@type='email']")]
        public IWebElement inputEmail { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement inputPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        public IWebElement pressBtnLogIn { get; set; }
    }
}
