﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace SeleniumTest.PageObjects
{
    public class RegistrationPage1
    {
        IWebDriver chrome;
        private readonly By btnSignUp = By.ClassName("Navbar__signUp--12ZDV");

        public RegistrationPage1(IWebDriver chrome)
        {
            this.chrome = chrome;
        }

        public AutorizationPage SignUp()
        {
            chrome.FindElement(btnSignUp).Click();
            return new AutorizationPage(chrome);
        }

        //[FindsBy(How = How.ClassName, Using = "Navbar__signUp--12ZDV")]
        //public IWebElement btnSignUp { get; set; }

        //[FindsBy(How = How.Name, Using = "first_name")]
        //public IWebElement firstName { get; set; }

        //[FindsBy(How = How.Name, Using ="last_name")]
        //public IWebElement lastName { get; set; }

        //[FindsBy(How = How.Name, Using = "email")]
        //public IWebElement email { get; set; }

        //[FindsBy(How = How.Name, Using = "password")]
        //public IWebElement password { get; set; }

        //[FindsBy(How = How.Name, Using = "password_confirm")]
        //public IWebElement passwordConfirm { get; set; }

        //[FindsBy(How = How.Name, Using = "phone_number")]
        //public IWebElement phoneNumber { get; set; }

        //[FindsBy(How = How.XPath, Using = "//button[@type='submit']")]
        //public IWebElement btnSubmit { get; set; }
    }

}
